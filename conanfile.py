from conans import ConanFile, tools


class ConcurrentqueueConan(ConanFile):
    name = "concurrentqueue"
    version = "git"
    license = "https://github.com/cameron314/concurrentqueue/blob/master/README.md"
    url = "https://github.com/cameron314/concurrentqueue"
    no_copy_source = True

    def source(self):
        self.run("git clone https://github.com/cameron314/concurrentqueue.git")

    def package(self):
        self.copy("blockingconcurrentqueue.h", dst="include", src="concurrentqueue")
        self.copy("concurrentqueue.h", dst="include", src="concurrentqueue")
        self.copy("lightweightsemaphore.h", dst="include", src="concurrentqueue")
